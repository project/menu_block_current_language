<?php

declare(strict_types = 1);

namespace Drupal\menu_block_current_language\Event;

use Drupal\Core\Menu\MenuLinkInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Translation event.
 */
class HasTranslationEvent extends Event {

  /**
   * HasTranslationEvent constructor.
   *
   * @param \Drupal\Core\Menu\MenuLinkInterface $link
   *   The menu link.
   * @param bool $status
   *   The visibility.
   */
  public function __construct(
    protected MenuLinkInterface $link,
    protected bool $status) {
  }

  /**
   * Determines if link should be visible or not.
   *
   * @return bool
   *   The visibility.
   */
  public function hasTranslation() : bool {
    return $this->status;
  }

  /**
   * Sets the visibility.
   *
   * @param bool $status
   *   The visibility.
   *
   * @return $this
   */
  public function setHasTranslation(bool $status) : self {
    $this->status = (bool) $status;
    return $this;
  }

  /**
   * Gets the menu link.
   *
   * @return \Drupal\Core\Menu\MenuLinkInterface
   *   The menu link.
   */
  public function getLink() : MenuLinkInterface {
    return $this->link;
  }

  /**
   * Sets the menu link.
   *
   * @param \Drupal\Core\Menu\MenuLinkInterface $menu_link
   *   The menu link.
   *
   * @return $this
   */
  public function setLink(MenuLinkInterface $menu_link) : self {
    $this->link = $menu_link;
    return $this;
  }

}
