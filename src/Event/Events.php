<?php

declare(strict_types = 1);

namespace Drupal\menu_block_current_language\Event;

/**
 * Event dispatcher events.
 */
final class Events {

  /**
   * Allows users to alter visibility of a menu link.
   */
  public const HAS_TRANSLATION = 'menu_block_current_language.has_translation';

}
