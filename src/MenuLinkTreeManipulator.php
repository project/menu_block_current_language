<?php

declare(strict_types = 1);

namespace Drupal\menu_block_current_language;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Menu\MenuLinkDefault;
use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\locale\StringStorageInterface;
use Drupal\menu_block_current_language\Event\Events;
use Drupal\menu_block_current_language\Event\HasTranslationEvent;
use Drupal\menu_link_content\Plugin\Menu\MenuLinkContent;
use Drupal\views\Plugin\Menu\ViewsMenuLink;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * A menu link tree manipulator.
 */
class MenuLinkTreeManipulator {

  /**
   * MenuLinkTreeManipulator constructor.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   * @param \Drupal\locale\StringStorageInterface $localeStorage
   *   The locale storage.
   */
  public function __construct(
    protected LanguageManagerInterface $languageManager,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected ConfigFactoryInterface $configFactory,
    protected EventDispatcherInterface $eventDispatcher,
    protected StringStorageInterface $localeStorage) {
  }

  /**
   * Load entity with given menu link.
   *
   * @param \Drupal\menu_link_content\Plugin\Menu\MenuLinkContent $link
   *   The menu link.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Boolean if menu link has no metadata. NULL if entity not found and
   *   an EntityInterface if found.
   */
  protected function getEntity(MenuLinkContent $link) : ? EntityInterface {
    // MenuLinkContent::getEntity() has protected visibility and cannot be used
    // to directly fetch the entity.
    $metadata = $link->getMetaData();

    if (empty($metadata['entity_id'])) {
      return NULL;
    }
    return $this->entityTypeManager
      ->getStorage('menu_link_content')
      ->load($metadata['entity_id']);
  }

  /**
   * Filter out links that are not translated to the current language.
   *
   * @param \Drupal\Core\Menu\MenuLinkTreeElement[] $tree
   *   The menu link tree to manipulate.
   * @param array $providers
   *   The menu block translatable link types.
   *
   * @return \Drupal\Core\Menu\MenuLinkTreeElement[]
   *   The manipulated menu link tree.
   */
  public function filterLanguages(array $tree, array $providers = []) : array {
    foreach ($tree as $index => $item) {
      // Handle expanded menu links.
      if ($item->hasChildren) {
        $item->subtree = $this->filterLanguages($item->subtree, $providers);
      }
      $link = $item->link;

      // MenuLinkDefault links have no common provider so fallback to 'default'.
      $provider = $link instanceof MenuLinkDefault ? 'default' : $link->getProvider();
      // Skip checks for disabled core providers. Isset check is used
      // to determine whether provider should be checked and empty whether
      // the provider is enabled or not (0 = disabled).
      if (isset($providers[$provider]) && empty($providers[$provider])) {
        continue;
      }
      $hasTranslation = $this->hasTranslation($link);
      /** @var \Drupal\menu_block_current_language\Event\HasTranslationEvent $event */
      // Allow other modules to determine visibility as well.
      $event = $this->eventDispatcher
        ->dispatch(new HasTranslationEvent($link, $hasTranslation), Events::HAS_TRANSLATION);

      if ($event->hasTranslation() === FALSE) {
        $tree[$index]->access = AccessResult::forbidden();
      }
    }
    return $tree;
  }

  /**
   * Checks if the given menu link has translation.
   *
   * @param \Drupal\Core\Menu\MenuLinkInterface $link
   *   The menu link.
   *
   * @return bool
   *   TRUE if the link has translation, FALSE if not.
   */
  protected function hasTranslation(MenuLinkInterface $link) : bool {
    $currentLanguage = $this->languageManager
      ->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)
      ->getId();

    if ($link instanceof MenuLinkContent) {
      return $this->hasMenuLinkContentTranslation($link, $currentLanguage);
    }

    if ($link->getPluginDefinition()['title'] instanceof TranslatableMarkup) {
      return $this->hasStringTranslation($link, $currentLanguage);
    }

    if ($link instanceof ViewsMenuLink) {
      return $this->hasViewsLinkTranslation($link, $currentLanguage);
    }

    if ($link instanceof MenuLinkTranslatableInterface) {
      return $link->hasTranslation($currentLanguage);
    }
    return TRUE;
  }

  /**
   * Checks if the given Views menu link should be visible.
   *
   * @param \Drupal\views\Plugin\Menu\ViewsMenuLink $link
   *   The Views menu link.
   * @param string $currentLanguage
   *   The current language.
   *
   * @return bool
   *   TRUE if the link should be visible, FALSE if not.
   */
  protected function hasViewsLinkTranslation(ViewsMenuLink $link, string $currentLanguage) : bool {
    $viewsId = sprintf('views.view.%s', $link->getMetaData()['view_id']);

    // Make sure the original configuration exists for given view.
    if (!$original = $this->configFactory->get($viewsId)->get('langcode')) {
      return TRUE;
    }
    // ConfigurableLanguageManager::getLanguageConfigOverride() always
    // returns a new configuration override for the original language.
    if ($currentLanguage === $original) {
      return TRUE;
    }
    /** @var \Drupal\language\Config\LanguageConfigOverride $config */
    $config = $this->languageManager->getLanguageConfigOverride($currentLanguage, $viewsId);

    // Configuration override will be marked as new if one does not
    // exist for the current language, thus has no translation.
    return !$config->isNew();
  }

  /**
   * Checks if menu link content should be visible or not.
   *
   * @param \Drupal\menu_link_content\Plugin\Menu\MenuLinkContent $link
   *   The menu link content.
   * @param string $currentLanguage
   *   The current language.
   *
   * @return bool
   *   TRUE if link should be visible, FALSE if not.
   */
  protected function hasMenuLinkContentTranslation(MenuLinkContent $link, string $currentLanguage) : bool {
    /** @var \Drupal\menu_link_content\Entity\MenuLinkContent $entity */
    if (!$entity = $this->getEntity($link)) {
      return FALSE;
    }

    // Do nothing if entity is not translatable.
    if (!$entity->isTranslatable()) {
      return TRUE;
    }
    return $entity->hasTranslation($currentLanguage);
  }

  /**
   * Check if given string has a string translation.
   *
   * @param \Drupal\Core\Menu\MenuLinkInterface $link
   *   The menu link.
   * @param string $currentLanguage
   *   The current langcode.
   *
   * @return bool
   *   TRUE if found translation, FALSE if not.
   */
  protected function hasStringTranslation(MenuLinkInterface $link, string $currentLanguage) : bool {
    $markup = $link->getPluginDefinition()['title'];
    // Skip this check for source language.
    // @todo This might cause some issues if string source language is not english.
    if ($currentLanguage === $this->languageManager->getDefaultLanguage()->getId()) {
      return TRUE;
    }
    $conditions = [
      'language' => $currentLanguage,
      'translated' => TRUE,
    ];
    $translations = $this->localeStorage->getTranslations($conditions, [
      'filters' => ['source' => $markup->getUntranslatedString()],
    ]);

    /** @var \Drupal\locale\TranslationString $translation */
    foreach ($translations as $translation) {
      if ($translation->isNew()) {
        continue;
      }
      // Make sure source strings are identical as getTranslations()
      // load strings with wildcard (%string%).
      if ($translation->source == $markup->getUntranslatedString()) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
